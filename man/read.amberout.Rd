\name{read.amberout}
\alias{read.amberout}
\alias{read.amberout.files}
\title{ Read AMBER MDOUT file(s) }
\description{
  Parse the content of AMBER MDOUT to a date.frame
}
\usage{
read.amberout(file, verbose=TRUE)

read.amberout.files(files=NULL, prefix=NULL, ext="out", sort=TRUE, \dots)

}
\arguments{
  \item{file}{ a single element character vector containing the name of
    the MDOUT file to be read. }
  \item{verbose}{ logical, if TRUE details are printed to screen. }
  \item{files}{ a character vector containing the name of
    the MDOUT files to be read. }
  \item{prefix}{ a single element character vector containing the prefix
    of the files to search for (e.g. \sQuote{production} in the case
    where files are names \sQuote{production1.out},
    \sQuote{production2.out}, etc. }
  \item{ext}{ a single element character vector containing the file
    extension. If a \sQuote{.} is not the first character it will be
    added internally. }
  \item{sort}{ logical, if TRUE files are sorted according to the number
    seperating \sQuote{prefix} and \sQuote{ext}. Usefull when file names
    are in the form \sQuote{prefix_9.out} \sQuote{prefix_10.out}, etc. }
  \item{...}{ arguments passed to \code{read.amberout}. }
}
\details{
  This function provides basic functionality to read and parse a AMBER
  MDOUT files. The resulting \sQuote{data.frame} object contains a complete
  list object of section 4 (\sQuote{RESULTS}) section in the AMBER MDOUT
  file.

  Note that \sQuote{prefix} should contain the complete prefix and not
  only a part of it. e.g. if the MDOUT files are named
  \sQuote{prod1.out}, \sQuote{prod2.out}, \sQuote{prod3.out}, the
  \sQuote{prefix} should be \sQuote{prod}. This will enable sorting of
  files with names \sQuote{prod9.out}, \sQuote{prod10.out}, etc. 

  See examples for further details.
}
\value{
  Returns a list of a data.frame columns according to the content present
  in the MDOUT file.

  Selected components:
  \item{nstep}{ a numeric vector of step information, i.e. the
    \sQuote{NSTEP} field in the MDOUT file. }
  \item{etot}{ a numeric vector containing the total energy at each
    nstep. }
  \item{ektot}{ a numeric vector of the kinetic energy at each nstep. }
}
\references{
  Grant, B.J. et al. (2006) \emph{Bioinformatics} \bold{22}, 2695--2696.
  \url{http://ambermd.org/formats.html}
}
\author{ Lars Skjaerven }
\seealso{
  \code{\link{read.prmtop}}, 
  \code{\link{read.crd}}, \code{\link{read.ncdf}},
}
\examples{
\dontrun{
## read a single MDOUT
out <- read.amberout("prod1.out")

## read multiple MDOUT files
out <- read.amberout.files(prefix="prod")

## access data
head(out$nstep)

## plot potential energy
plot(out$eptot)
}
}
\keyword{ IO }
