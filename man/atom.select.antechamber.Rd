\name{atom.select.antechamber}
\alias{atom.select.antechamber}
\title{ Atom Selection from ANTECHAMBER Structure Objects }
\description{
  Return the \sQuote{atom}, \sQuote{xyz} and \sQuote{bond} indices of
  \sQuote{antechamber}structure objects corresponding to the
  intersection of a hierarchical selection.
}
\usage{
\method{atom.select}{antechamber}(ac, eleno = NULL, atna = NULL,
                                 resid = NULL, verbose = TRUE, \dots)
}
\arguments{
  \item{ac}{ a structure object of class \code{"antechamber"}, obtained from
    \code{\link{read.ac}}. }
  \item{eleno}{ a numeric vector of element numbers. }
  \item{atna}{ a character vector of atom names. }
  \item{resid}{ a character vector of residue name identifiers. }
  \item{verbose}{ logical, if TRUE details of the selection are printed. }
  \item{...}{ arguments passed to and from functions. }
}
\details{
  Note: this is a wrapper function calling the underlying functions
  \code{as.pdb} and \code{atom.select.pdb}. 
}
\value{
  Returns a list of class \code{"select"} with the following components:
  \item{atom}{ a numeric vector of atomic indices. }
  \item{xyz }{ a numeric vector of xyz indices. }
  \item{bond}{ a numeric vector of bond indices. }
  \item{call }{ the matched call. }
}
\references{
  Grant, B.J. et al. (2006) \emph{Bioinformatics} \bold{22}, 2695--2696.
}
\author{ Lars Skjaerven }
\seealso{
  \code{\link{atom.select}}, \code{\link{as.select}},
  \code{\link{combine.select}}, 
  \code{\link{read.ac}}, \code{\link{trim.antechamber}},
}
\keyword{utilities}

