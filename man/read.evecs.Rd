\name{read.evecs}
\alias{read.evecs}
\title{ Read AMBER Eigenvector (evecs) files }
\description{
  Read eigenvector data from an AMBER coordinate file.
}
\usage{
read.evecs(file, ...)
}
\arguments{
  \item{file}{ name of evecs file to read. }
  \item{\dots}{ arguments passed to and from functions. }
}
\details{
  Read a AMBER eigenvector (evecs) format file.
}
\value{
  A list object of type \sQuote{amber} and \sQuote{evecs} with the
  following components:
  \item{title}{ title. }
  \item{xyz}{ a numeric matrix of class \sQuote{xyz} containing the
    provided Cartesian coordinates. }
  \item{vectors}{ a numeric matrix containing the eigenvectors. }
  \item{values}{ numeric vector containing the eigenvalues. }
  \item{nmodes}{ total number of modes of the system.  }
  \item{call}{ the matched call. }
}
\references{
  Grant, B.J. et al. (2006) \emph{Bioinformatics} \bold{22}, 2695--2696.
  \url{http://ambermd.org/formats.html}
}
\author{ Lars Skjaerven }
\note{
  See AMBER documentation for Coordinate format description.
}
\seealso{
  \code{\link{read.prmtop}} }
\examples{
\dontrun{
## Read a PRMTOP file
evecs <- read.evecs("vecs")

## print the eigenvalues
evecs$values
}
}
\keyword{ IO }
