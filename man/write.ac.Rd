\name{write.ac}
\alias{write.ac}
\title{ Write AMBER ANTECHAMBER Format Structure File }
\description{
  Write a AMBER ANTECHAMBER (AC) file.
}
\usage{
write.ac(ac, file = "R.ac")
}
\arguments{
  \item{ac}{ a AC structure object, e.g. obtained from
    \code{\link{read.ac}}. } 
  \item{file}{ the output file name. }
}
\value{
  Called for its effect.
}
\references{
  Grant, B.J. et al. (2006) \emph{Bioinformatics} \bold{22}, 2695--2696.
}
\author{
  Lars Skjaerven
}
\seealso{
  \code{\link{read.ac}}, \code{\link{trim.antechamber}},
}
\examples{
\donttest{
## Read a AC file
ac <- read.ac("ANTECHAMBER_PREP.AC")

## Select calpha atoms
sele <- atom.select(ac, resid="H4B")

## Trim 
new <- trim(ac, inds=sele)

## Write new AC file
write.ac(new)
}
}
\keyword{ IO }
