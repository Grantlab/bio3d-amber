read.ac <- function (file, verbose=TRUE) {
  
  if(missing(file)) {
    stop("please specify a AC 'file' for reading")
  }
  
  cl <- match.call()
  
  ## PDB FORMAT v3.3:    colpos, datatype,   name,       description
  atom.format <- matrix(c(6,    'character', "type",     # type(ATOM)
                          5,     'numeric',   "eleno",   # atom_no
                         -1,     NA,          NA,        # (blank)
                          5,     'character', "atna",    # atom name
                          3,     'character', "resid",   # res_na 
                         -1,     NA,          NA,        # blank
                          5,     'numeric',   "resno",   # res_no
                         -4,     NA,           NA,       # (blank)
                          8,     'numeric',   "x",       # x
                          8,     'numeric',   "y",       # y
                          8,     'numeric',   "z",       # z
                          10,    'numeric', "charge",    # charge
                          -3,    NA,           NA,       # (blank)
                          7,    'character',  "atty"     # atom type
                          ), ncol=3, byrow=TRUE,
                        dimnames = list(c(1:14), c("widths","what","name")) )

  bond.format <- matrix(c(4,    'character',  "type",     # type(ATOM)
                          5,     'numeric',   "bondno",   # bond_no
                          5,     'numeric',   "a",        # atom_a
                          5,     'numeric',   "b",        # atom_b
                          5,     'numeric',   "bondty",   # bond_type
                         -2,     NA,           NA,        # (blank)
                          5,     'character', "at.a",     # name of atom a
                          5,     'character', "at.b"      # name of atom b
                          ), ncol=3, byrow=TRUE,
                        dimnames = list(c(1:8), c("widths","what","name")) )

  
  trim <- function(s) {
    ##- Remove leading and trailing spaces from character strings
    s <- sub("^ +", "", s)
    s <- sub(" +$", "", s)
    s[(s=="")]<-NA
    s
  }

  split.fields <- function(x) {
     ##- Split a character string for data.frame fwf reading
     ##  First splits a string 'x' according to 'first' and 'last'
     ##  then re-combines to new string with "," as separator 
     x <- trim( substring(x, first, last) )
     paste(x,collapse=",")
  }

  is.character0 <- function(x){length(x)==0 & is.character(x)}


  ##- Find first and last (substr) positions for each field
  widths <-  as.numeric(atom.format[,"widths"]) # fixed-width spec
  drop.ind <- (widths < 0) # cols to ignore (i.e. -ve)
  widths <- abs(widths)    # absolute vales for later
  st <- c(1, 1 + cumsum( widths ))
  first <- st[-length(st)][!drop.ind] # substr start
  last <- cumsum( widths )[!drop.ind] # substr end
  names(first) = na.omit(atom.format[,"name"])
  names(last) = names(first)

  ##- Read 'n' lines of PDB file
  raw.lines  <- readLines(file)
  type <- substring(raw.lines, first["type"], last["type"])

  ##- Split input lines by record type
  raw.header <- raw.lines[type == "CHARGE"]
  ##raw.formul <- raw.lines[type == "Formul"]
  raw.atom  <- raw.lines[type == "ATOM  "]
  raw.bond  <- raw.lines[type == "BOND  "]


  atom <- read.table(text=sapply(raw.atom, split.fields),
                    stringsAsFactors=FALSE, sep=",", quote='',
                    colClasses=atom.format[!drop.ind,"what"],
                    col.names=atom.format[!drop.ind,"name"],
                    comment.char="")
  
  ##- Find first and last (substr) positions for each field
  widths <-  as.numeric(bond.format[,"widths"]) # fixed-width spec
  drop.ind <- (widths < 0) # cols to ignore (i.e. -ve)
  widths <- abs(widths)    # absolute vales for later
  st <- c(1, 1 + cumsum( widths ))
  first <- st[-length(st)][!drop.ind] # substr start
  last <- cumsum( widths )[!drop.ind] # substr end
  names(first) = na.omit(bond.format[,"name"])
  names(last) = names(first)

  bond <- read.table(text=sapply(raw.bond, split.fields),
                    stringsAsFactors=FALSE, sep=",", quote='',
                    colClasses=bond.format[!drop.ind,"what"],
                    col.names=bond.format[!drop.ind,"name"],
                    comment.char="")

  ##- Coordinates only object
  xyz.models <- matrix(as.numeric(c(t(atom[,c("x","y","z")]))), nrow=1)
  rm(raw.lines, raw.atom, raw.bond)


  output<-list(atom=atom,
               bond=bond, 
               xyz=as.xyz(xyz.models),
               call=cl)
  class(output) <- "antechamber"
  
  return(output)
}
