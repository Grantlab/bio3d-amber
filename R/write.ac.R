
"write.ac" <- function(ac, file="R.ac") {
  #if(!is.sdf(sdf))
  #  stop("input should be of class 'sdf' as obtained by 'read.sdf'")

  elements <- table(atom2ele(ac$atom$atna))
  frm <- paste(paste(names(elements), elements, sep=""), collapse=" ")

  

  chg <- sum(ac$atom$charge)
  fmt <- "%8.2f"
  chg <- paste("CHARGE  ", sprintf(fmt, chg), " ( ", round(chg, 0), " )", sep="")
  frm <- paste("Formula: ", frm, sep="")
    
  raw.lines <- c()
  raw.lines <- c(raw.lines, chg)
  raw.lines <- c(raw.lines, frm)
  
  fmt <- "%-6s%5s  %-4s%3s %5s    %8.3f%8.3f%8.3f%10.6f   %7s"
  for ( i in 1:nrow(ac$atom) ) {
    
    raw.lines <- c(raw.lines, 
               sprintf(fmt, ac$atom[i, 1], ac$atom[i, 2],
                       ac$atom[i, 3], ac$atom[i, 4],
                       ac$atom[i, 5], ac$atom[i, 6],
                       ac$atom[i, 7], ac$atom[i, 8],
                       ac$atom[i, 9], ac$atom[i, 10])
                   )
  }
  
  
  if(length(ac$bond)>0) {
    fmt <- "%4s%5s%5s%5s%5s  %5s%5s"
    for ( i in 1:nrow(ac$bond) ) {
      raw.lines <- c(raw.lines, 
                     sprintf(fmt,
                             ac$bond[i, 1], ac$bond[i, 2],
                             ac$bond[i, 3], ac$bond[i, 4],
                             ac$bond[i, 5], ac$bond[i, 6],
                           ac$bond[i, 7])
                     )
    }
  }
  else {
    ac$bond <- NULL
  }
  
  write.table(raw.lines, file = file, quote = FALSE, 
              row.names = FALSE, col.names = FALSE, append=FALSE)
}
